package com.nightowlstudios.mynotepad;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FAQfragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FAQfragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FAQfragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Aboutfragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FAQfragment newInstance(String param1, String param2) {
        FAQfragment fragment = new FAQfragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
BottomNavigationView bottomNavigationViewx;
    TextView qn1,qn2,qn3,qn4,qn5,qn6,qn7;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview=inflater.inflate(R.layout.fragment_faq, container, false);
        // Inflate the layout for this fragment
        bottomNavigationViewx=rootview.findViewById(R.id.bottomnavfaq);
        qn1=rootview.findViewById(R.id.question1);
        qn2=rootview.findViewById(R.id.question2);
        qn3=rootview.findViewById(R.id.question3);
        qn4=rootview.findViewById(R.id.question4);
        qn5=rootview.findViewById(R.id.question5);
        qn6=rootview.findViewById(R.id.question6);
        qn7=rootview.findViewById(R.id.question7);
        bottomNavigationViewx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.back:

                    case R.id.home1:

                        Intent intent1=new Intent(getActivity(),NavigationActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.exitapp:

                        callAlertbar();

                        break;

                }

                return true;
            }
        });
        qn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder question1=new AlertDialog.Builder(getActivity());
                question1.setTitle("How to share notes?");
                question1.setMessage(R.string.question1);
                question1.setCancelable(true);
                question1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                question1.create();
                question1.show();
            }
        });
        qn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder question2=new AlertDialog.Builder(getActivity());
                question2.setTitle("How to delete notes?");
                question2.setMessage(R.string.question2);
                question2.setCancelable(true);
                question2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                question2.create();
                question2.show();
            }
        });
        qn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder question3=new AlertDialog.Builder(getActivity());
                question3.setTitle("How to set events on calendar?");
                question3.setMessage(R.string.question3);
                question3.setCancelable(true);
                question3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                question3.create();
                question3.show();
            }
        });
        qn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder question4=new AlertDialog.Builder(getActivity());
                question4.setTitle("How to view events?");
                question4.setMessage(R.string.question4);
                question4.setCancelable(true);
                question4.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                question4.create();
                question4.show();
            }
        });
        qn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder question5=new AlertDialog.Builder(getActivity());
                question5.setTitle("How to search holidays?");
                question5.setMessage(R.string.question5);
                question5.setCancelable(true);
                question5.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                question5.create();
                question5.show();
            }
        });
        qn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder question6=new AlertDialog.Builder(getActivity());
                question6.setTitle("How to contact us?");
                question6.setMessage(R.string.question6);
                question6.setCancelable(true);
                question6.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                question6.create();
                question6.show();
            }
        });
        qn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"opening your gmail account",Toast.LENGTH_LONG).show();
                Intent contactintent=new Intent(Intent.ACTION_SEND);
                String[] recipients={"faizahmmd37@gmail.com"};
                String[] cc_recipient={"nightowlstudioz@gmail.com"};
                contactintent.putExtra(Intent.EXTRA_CC,cc_recipient);
                contactintent.putExtra(Intent.EXTRA_EMAIL,recipients);
                contactintent.putExtra(Intent.EXTRA_SUBJECT,"Contact us form");
                contactintent.putExtra(Intent.EXTRA_TEXT,"Write your message here...");

                contactintent.setType("text/html");
                contactintent.setPackage("com.google.android.gm");
                startActivity(Intent.createChooser(contactintent,"send email"));
            }
        });

        return rootview;

    }
    public void callAlertbar(){
        AlertDialog.Builder alertbar=new AlertDialog.Builder(getActivity());
        alertbar.setTitle("EXIT APP");
        alertbar.setMessage("Do you really want to exit?");
        alertbar.setCancelable(true);
        alertbar.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        alertbar.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertbar.create();
        alertbar.show();
    }}
