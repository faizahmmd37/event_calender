package com.nightowlstudios.mynotepad;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;

public class myAdapter extends BaseAdapter implements Filterable {
    ArrayList<String> arrayList;
    Context context;
    ArrayList<String> backup;

    public myAdapter(ArrayList<String> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        backup=new ArrayList<>(arrayList);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {
        myViewHolder myviewholder;
        if (convertview==null) {
            myviewholder=new myViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            convertview = layoutInflater.inflate(R.layout.singlerow, viewGroup, false);
            myviewholder.textView = convertview.findViewById(R.id.textview);
convertview.setTag(myviewholder);
        }else
        {
        myviewholder=(myViewHolder)convertview.getTag();
        }
        myviewholder.textView.setText(arrayList.get(i).toString());
        return convertview;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
Filter filter=new Filter() {
    @Override
    protected FilterResults performFiltering(CharSequence keyword) {
ArrayList<String> filteredarray=new ArrayList<>();
if (keyword.toString().isEmpty()){
    filteredarray.addAll(backup);
}else {
for (String obj:backup){
    if (obj.toLowerCase().contains(keyword.toString().toLowerCase())){
        filteredarray.add(obj);
    }
}
}
FilterResults filterResults=new FilterResults();
filterResults.values=filteredarray;
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
arrayList.clear();
arrayList.addAll((ArrayList<String>)filterResults.values);
notifyDataSetChanged();
    }
};
    public static class myViewHolder{
        TextView textView;



    }
}
