package com.nightowlstudios.mynotepad;

public class Events {
    private String Event,Date,Month,Year,Time;

    public Events(String event, String time, String date, String month, String year) {
        Event = event;
        Date = date;
        Month = month;
        Year = year;
        Time = time;
    }

    public String getEvent() {
        return Event;
    }

    public void setEvent(String event) {
        Event = event;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getMonth() {
        return Month;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
