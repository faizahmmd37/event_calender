package com.nightowlstudios.mynotepad;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.EventLog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EventRecyclerAdapter extends RecyclerView.Adapter<EventRecyclerAdapter.MyViewHolder> {
Context context;
ArrayList<Events> arrayList;
mysqLiteOpenHelper mysqliteopenhelper;

    public EventRecyclerAdapter(Context context, ArrayList<Events> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.show_events_rowlayout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
final Events events=arrayList.get(position);
holder.Event.setText(events.getEvent());
holder.DateTxt.setText(events.getDate());
holder.Time.setText(events.getTime());
holder.delete.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        deleteCalenderEvent(events.getEvent(),events.getDate(),events.getTime());
        arrayList.remove(position);
        notifyDataSetChanged();
    }
});
if (isAlarmed(events.getDate(),events.getEvent(),events.getTime())){
    holder.setAlarm.setImageResource(R.drawable.ic_action_notification_on);

}else {
    holder.setAlarm.setImageResource(R.drawable.ic_action_notification_off);

}
Calendar datecalendar=Calendar.getInstance();
datecalendar.setTime(ConvertStringtoDate(events.getDate()));
final int alarmYear=datecalendar.get(Calendar.YEAR);
final int alarmMonth=datecalendar.get(Calendar.MONTH);
final int alarmDay=datecalendar.get(Calendar.DAY_OF_MONTH);
        Calendar timecalendar=Calendar.getInstance();
        timecalendar.setTime(ConvertStringtoTime(events.getTime()));
        final int alarmHour=timecalendar.get(Calendar.HOUR_OF_DAY);
        final int alarmMinute=timecalendar.get(Calendar.MINUTE);


holder.setAlarm.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if (isAlarmed(events.getDate(),events.getEvent(),events.getTime())){
            holder.setAlarm.setImageResource(R.drawable.ic_action_notification_off);
            cancelAlarm(getRequestCode(events.getDate(),events.getEvent(),events.getTime()));
            updateEvent(events.getDate(),events.getEvent(),events.getTime(),"off");
            notifyDataSetChanged();
        }else {
            holder.setAlarm.setImageResource(R.drawable.ic_action_notification_on);
            Calendar alarmCalender=Calendar.getInstance();
            alarmCalender.set(alarmYear,alarmMonth,alarmDay,alarmHour,alarmMinute);
            setAlarm(alarmCalender,events.getEvent(),events.getTime(),getRequestCode(events.getDate(),events.getEvent(),events.getTime()));
            updateEvent(events.getDate(),events.getEvent(),events.getTime(),"on");
            notifyDataSetChanged();
        }
    }
});
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView DateTxt,Event,Time;
        Button delete;
        ImageButton setAlarm;
        public MyViewHolder(@NonNull View itemView){
            super(itemView);
            DateTxt=itemView.findViewById(R.id.eventdate);
            Event=itemView.findViewById(R.id.eventname);
            Time=itemView.findViewById(R.id.eventtime);
            delete=itemView.findViewById(R.id.delete);
setAlarm=itemView.findViewById(R.id.alarmmeBtn);

        }
    }
    private Date ConvertStringtoDate(String eventDate){
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date=null;
        try {
            date=format.parse(eventDate);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return date;
    }
    private Date ConvertStringtoTime(String eventDate){
        SimpleDateFormat format=new SimpleDateFormat("kk:mm", Locale.ENGLISH);
        Date date=null;
        try {
            date=format.parse(eventDate);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return date;
    }

    private void deleteCalenderEvent(String event,String date,String time){
    mysqliteopenhelper=new mysqLiteOpenHelper(context);
    SQLiteDatabase database=mysqliteopenhelper.getWritableDatabase();
    mysqliteopenhelper.deleteEvent(event,date,time,database);
    database.close();
}
private boolean isAlarmed(String date,String event,String time){
        boolean alarmed=false;
    mysqliteopenhelper=new mysqLiteOpenHelper(context);
    SQLiteDatabase database=mysqliteopenhelper.getReadableDatabase();
    Cursor cursor=mysqliteopenhelper.readIDEvents(date,event,time,database);
    while (cursor.moveToNext())
    {
        String notify=cursor.getString(cursor.getColumnIndex(DBStructure.Notify));
        if (notify.equals("on")){
            alarmed=true;
        }else {
            alarmed=false;
        }


    }
    cursor.close();
    mysqliteopenhelper.close();
    return alarmed;
}
    private void setAlarm(Calendar calendar, String event, String time, int RequestCode){
        Intent intent=new Intent(context.getApplicationContext(),AlarmReceiver.class);
        intent.putExtra("event",event);
        intent.putExtra("time",time);
        intent.putExtra("id",RequestCode);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(context,RequestCode,intent,PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager=(AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);

    }
    private void cancelAlarm(int RequestCode){
        Intent intent=new Intent(context.getApplicationContext(),AlarmReceiver.class);

        PendingIntent pendingIntent=PendingIntent.getBroadcast(context,RequestCode,intent,PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager=(AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

    }
    private int getRequestCode(String date,String event,String time){
        int code=0;
        mysqliteopenhelper=new mysqLiteOpenHelper(context);
        SQLiteDatabase database=mysqliteopenhelper.getReadableDatabase();
        Cursor cursor=mysqliteopenhelper.readIDEvents(date,event,time,database);
        while (cursor.moveToNext())
        {
            code=cursor.getInt(cursor.getColumnIndex(DBStructure.ID));


        }
        cursor.close();
        mysqliteopenhelper.close();
        return code;
    }
    private void updateEvent(String date,String event,String time,String notify){
        mysqliteopenhelper=new mysqLiteOpenHelper(context);
        SQLiteDatabase database=mysqliteopenhelper.getWritableDatabase();
        mysqliteopenhelper.updateEvent(date,event,time,notify,database);
        mysqliteopenhelper.close();
    }
}
