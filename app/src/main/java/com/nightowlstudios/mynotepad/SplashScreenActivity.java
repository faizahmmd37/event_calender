package com.nightowlstudios.mynotepad;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {
ImageView img1;
TextView img2;
Animation top,bottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        img1=findViewById(R.id.imageview1);
        img2=findViewById(R.id.imageview2);
        top= AnimationUtils.loadAnimation(SplashScreenActivity.this,R.anim.logoicon);
        bottom=AnimationUtils.loadAnimation(SplashScreenActivity.this,R.anim.logotitle);
        img1.setAnimation(top);
        img2.setAnimation(bottom);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
       //getSupportActionBar().hide();

        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashScreenActivity.this,NavigationActivity.class);
                startActivity(intent);
            }
        },2000);

    }
}