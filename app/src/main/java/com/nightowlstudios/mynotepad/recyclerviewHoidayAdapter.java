package com.nightowlstudios.mynotepad;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class recyclerviewHoidayAdapter extends RecyclerView.Adapter<viewHolderHolidays> implements Filterable {
ArrayList<ModelHoliday> data;
ArrayList<ModelHoliday> backup;
Context context;

    public recyclerviewHoidayAdapter(ArrayList<ModelHoliday> data,Context context) {
        this.data = data;
        this.context=context;
        backup=new ArrayList<>(data);

    }

    @NonNull
    @Override
    public viewHolderHolidays onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View convertview=layoutInflater.inflate(R.layout.singlerowholidayrecycle,parent,false);

        return new viewHolderHolidays(convertview);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderHolidays holder, int position) {
holder.txttitle.setText(data.get(position).getTitle());
holder.txtday.setText(data.get(position).getDay());
holder.txtdate.setText(data.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public Filter getFilter() {
        return filter;

    }
    Filter filter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            ArrayList<ModelHoliday> filteredobjArray=new ArrayList<>();
            if (charSequence.toString().isEmpty()){
                filteredobjArray.addAll(backup);
            }else {
                for (ModelHoliday objects:backup){
                    if (objects.getTitle().toString().toLowerCase().contains(charSequence.toString().toLowerCase())){
                        filteredobjArray.add(objects);
                    }
                }
            }
            FilterResults filterResults=new FilterResults();
            filterResults.values=filteredobjArray;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
data.clear();
data.addAll((ArrayList<ModelHoliday>)filterResults.values);
notifyDataSetChanged();
        }
    };
}
