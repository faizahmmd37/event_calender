package com.nightowlstudios.mynotepad;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class viewHolderHolidays extends RecyclerView.ViewHolder {
    TextView txttitle,txtday,txtdate;

    public viewHolderHolidays(@NonNull View itemView) {
        super(itemView);

        txttitle=(TextView)itemView.findViewById(R.id.textviewtitle);
        txtday=(TextView)itemView.findViewById(R.id.textviewday);
        txtdate=(TextView)itemView.findViewById(R.id.textviewdate);
    }



}
