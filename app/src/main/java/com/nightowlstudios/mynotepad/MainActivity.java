package com.nightowlstudios.mynotepad;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashSet;

public class MainActivity extends AppCompatActivity {
    EditText multilinetext;

    SharedPreferences sharedPreferences;
     int arraylistposition;
     static String string;
     FloatingActionButton floatingActionButton2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        multilinetext = findViewById(R.id.MultiLineText);
        floatingActionButton2=findViewById(R.id.sendnote);
        final ListView listView=findViewById(R.id.listView);

        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        arraylistposition = intent.getIntExtra("intentkey",-1);
        if (arraylistposition !=-1) {

            multilinetext.setText(ArrayActivity.arrayList.get(arraylistposition));

        }
        else
        {
            ArrayActivity.arrayList.add("");
            arraylistposition=ArrayActivity.arrayList.size()-1;
          // ArrayActivity.arrayAdapter.notifyDataSetChanged();
            ArrayActivity.myadapter.notifyDataSetChanged();

        }


        multilinetext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {




                ArrayActivity.arrayList.set(arraylistposition, String.valueOf(charSequence));
               
               // ArrayActivity.arrayAdapter.notifyDataSetChanged();
               ArrayActivity.myadapter.notifyDataSetChanged();

                SharedPreferences sharedPreferences=getSharedPreferences("sharedpreferencekey",MODE_PRIVATE);
                SharedPreferences.Editor editor= sharedPreferences.edit();
                HashSet<String> set=new HashSet<>(ArrayActivity.arrayList);
                editor.putStringSet("key",set);
                editor.apply();



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

floatingActionButton2.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent sendintent2=new Intent();
        sendintent2.setAction(Intent.ACTION_SEND);
        sendintent2.putExtra(Intent.EXTRA_TEXT,multilinetext.getText().toString());
        sendintent2.setType("text/plain");
        Intent.createChooser(sendintent2,"Send note via");
        startActivity(sendintent2);
    }
});
    }

}