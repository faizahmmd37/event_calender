package com.nightowlstudios.mynotepad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class HolidayMainActivity extends AppCompatActivity {

RecyclerView recyclerViewholiday;
    recyclerviewHoidayAdapter recyclerviewhoidayadapter;
    androidx.appcompat.widget.Toolbar toolbar4;
    BottomNavigationView bottomnavholidays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holiday_main);
        toolbar4=(Toolbar)findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar4);
        bottomnavholidays=findViewById(R.id.bottomnavholidays);

        recyclerViewholiday=findViewById(R.id.recyclerviewholiday);
LinearLayoutManager linearLayoutManager=new LinearLayoutManager(HolidayMainActivity.this);
linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerViewholiday.setLayoutManager(linearLayoutManager);

        recyclerviewhoidayadapter=new recyclerviewHoidayAdapter(addHoliday(),HolidayMainActivity.this);

        recyclerViewholiday.setAdapter(recyclerviewhoidayadapter);


        bottomnavholidays.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.back:

                    case R.id.home1:

                        Intent intent1=new Intent(HolidayMainActivity.this,NavigationActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.exitapp:

                        callAlertbar();

                        break;

                }

                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchholidaymenu,menu);
        MenuItem menuItem=menu.findItem(R.id.searchholidays);
        SearchView searchView=(SearchView)menuItem.getActionView();
        searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.queryhintholiday) + "</font>"));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
recyclerviewhoidayadapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public void callAlertbar(){
        AlertDialog.Builder alertbar=new AlertDialog.Builder(this);
        alertbar.setTitle("EXIT APP");
        alertbar.setMessage("Do you really want to exit?");
        alertbar.setCancelable(true);
        alertbar.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        alertbar.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertbar.create();
        alertbar.show();
    }
    public ArrayList<ModelHoliday> addHoliday(){
        ArrayList<ModelHoliday> arrayList=new ArrayList<>();


        ModelHoliday obj1=new ModelHoliday();
        obj1.setTitle("Mannam Jayanti(Kerala)");
        obj1.setDay("Thursday");
        obj1.setDate("2 January 2020");
        arrayList.add(obj1);

        ModelHoliday obj1A=new ModelHoliday();
        obj1A.setTitle("Guru Gobind Singh Jayanti(Haryana,Rajasthan)");
        obj1A.setDay("Thursday");
        obj1A.setDate("2 January 2020");
        arrayList.add(obj1A);

        ModelHoliday obj2=new ModelHoliday();
        obj2.setTitle("Swami Vivekananda Jayanthi(West Bengal)");
        obj2.setDay("Sunday");
        obj2.setDate("12 January 2020");
        arrayList.add(obj2);

        ModelHoliday obj3=new ModelHoliday();
        obj3.setTitle("Bhogi(Andra Pradesh,Telangana)");
        obj3.setDay("Wednesday");
        obj3.setDate("15 January 2020");
        arrayList.add(obj3);

        ModelHoliday obj3A=new ModelHoliday();
        obj3A.setTitle("Makar Sankranti(Several states)");
        obj3A.setDay("Wednesday");
        obj3A.setDate("15 January 2020");
        arrayList.add(obj3A);


        ModelHoliday obj3B=new ModelHoliday();
        obj3B.setTitle("Pongal(Several states)");
        obj3B.setDay("Wednesday");
        obj3B.setDate("15 January 2020");
        arrayList.add(obj3B);

        ModelHoliday obj3C=new ModelHoliday();
        obj3C.setTitle("Tusu Puja(Assam)");
        obj3C.setDay("Wednesday");
        obj3C.setDate("15 January 2020");
        arrayList.add(obj3C);

        ModelHoliday obj4=new ModelHoliday();
        obj4.setTitle("Thiruvallavur Day(Tamil Nadu,Puducherry)");
        obj4.setDay("Thursday");
        obj4.setDate("16 January 2020");
        arrayList.add(obj4);

        ModelHoliday obj4A=new ModelHoliday();
        obj4A.setTitle("Kanuma Pandunga(Andhra Pradesh)");
        obj4A.setDay("Thursday");
        obj4A.setDate("16 January 2020");
        arrayList.add(obj4A);


        ModelHoliday obj5=new ModelHoliday();
        obj5.setTitle("Netaji Subhash Chandra Bose Jayanti(Assam,West Bengal,Tripura,Odisha)");
        obj5.setDay("Thursday");
        obj5.setDate("23 January 2020");
        arrayList.add(obj5);

        ModelHoliday obj6=new ModelHoliday();
        obj6.setTitle("Sonam Losar(Sikkim)");
        obj6.setDay("Saturday");
        obj6.setDate("25 January 2020");
        arrayList.add(obj6);

        ModelHoliday obj6A=new ModelHoliday();
        obj6A.setTitle("Statehood(Himachal Pradesh)");
        obj6A.setDay("Saturday");
        obj6A.setDate("25 January 2020");
        arrayList.add(obj6A);

        ModelHoliday obj7=new ModelHoliday();
        obj7.setTitle("Republic Day(All over India)");
        obj7.setDay("Sunday");
        obj7.setDate("26 January 2020");
        arrayList.add(obj7);

        ModelHoliday obj8=new ModelHoliday();
        obj8.setTitle("Vasant Panchami(West Bengal,Tripura,Odisha,Haryana,Punjab)");
        obj8.setDay("Thursday");
        obj8.setDate("30 January 2020");
        arrayList.add(obj8);


        ModelHoliday obj9=new ModelHoliday();
        obj9.setTitle("Guru Ravidas Jayanti(Punjab,Haryana,Himachal Pradesh,Chandigarh)");
        obj9.setDay("Sunday");
        obj9.setDate("09 February 2020");
        arrayList.add(obj9);

        ModelHoliday obj10=new ModelHoliday();
        obj10.setTitle("Chatrapati Shivaji Maharaj Jayanti(Maharastra)");
        obj10.setDay("Wednesday");
        obj10.setDate("19 February 2020");
        arrayList.add(obj10);

        ModelHoliday obj11=new ModelHoliday();
        obj11.setTitle("Statehood Day(Arunachal Pradesh)");
        obj11.setDay("Thursday");
        obj11.setDate("20 February 2020");
        arrayList.add(obj11);


        ModelHoliday obj11A=new ModelHoliday();
        obj11A.setTitle("State Day(Mizoram)");
        obj11A.setDay("Thursday");
        obj11A.setDate("20 February 2020");
        arrayList.add(obj11A);

        ModelHoliday obj12=new ModelHoliday();
        obj12.setTitle("Maha Shivarathri(Several states)");
        obj12.setDay("Friday");
        obj12.setDate("21 February 2020");
        arrayList.add(obj12);

        ModelHoliday obj13=new ModelHoliday();
        obj13.setTitle("Panchayatiraj Divas(Odisha)");
        obj13.setDay("Thursday");
        obj13.setDate("05 March 2020");
        arrayList.add(obj13);

        ModelHoliday obj14=new ModelHoliday();
        obj14.setTitle("Chapchar(Mizoram)");
        obj14.setDay("Friday");
        obj14.setDate("06 March 2020");
        arrayList.add(obj14);

        ModelHoliday obj15=new ModelHoliday();
        obj15.setTitle("Hazrat Ali's birthday(Uttar Pradesh)");
        obj15.setDay("Sunday");
        obj15.setDate("08 March 2020");
        arrayList.add(obj15);

        ModelHoliday obj16=new ModelHoliday();
        obj16.setTitle("Holi(Several states)");
        obj16.setDay("Tuesday");
        obj16.setDate("10 March 2020");
        arrayList.add(obj16);

        ModelHoliday obj17=new ModelHoliday();
        obj17.setTitle("Bihar Divas(Bihar)");
        obj17.setDay("Sunday");
        obj17.setDate("22 March 2020");
        arrayList.add(obj17);

        ModelHoliday obj18=new ModelHoliday();
        obj18.setTitle("S.Bhagat Singh's Martyrdom Day(Punjab,Haryana)");
        obj18.setDay("Monday");
        obj18.setDate("23 March 2020");
        arrayList.add(obj18);

        ModelHoliday obj19=new ModelHoliday();
        obj19.setTitle("Ugadi(Andhra Pradesh,Telangana,Karnataka,Maharashtra)");
        obj19.setDay("Wednesday");
        obj19.setDate("25 March 2020");
        arrayList.add(obj19);

        ModelHoliday obj20=new ModelHoliday();
        obj20.setTitle("Odisha Day(Odisha)");
        obj20.setDay("Wednesday");
        obj20.setDate("01 April 2020");
        arrayList.add(obj20);

        ModelHoliday obj21=new ModelHoliday();
        obj21.setTitle("Ram Navami(Several states)");
        obj21.setDay("Thursday");
        obj21.setDate("02 April 2020");
        arrayList.add(obj21);

        ModelHoliday obj22=new ModelHoliday();
        obj22.setTitle("Mahavir Jayanti(Several states)");
        obj22.setDay("Monday");
        obj22.setDate("06 April 2020");
        arrayList.add(obj22);

        ModelHoliday obj23=new ModelHoliday();
        obj23.setTitle("Maundy Thursday(Kerala)");
        obj23.setDay("Thursday");
        obj23.setDate("09 April 2020");
        arrayList.add(obj23);

        ModelHoliday obj24=new ModelHoliday();
        obj24.setTitle("Good Friday(Several states)");
        obj24.setDay("Friday");
        obj24.setDate("10 April 2020");
        arrayList.add(obj24);

        ModelHoliday obj25=new ModelHoliday();
        obj25.setTitle("Easter Saturday(Nagaland)");
        obj25.setDay("Saturday");
        obj25.setDate("11 April 2020");
        arrayList.add(obj25);

        ModelHoliday obj26=new ModelHoliday();
        obj26.setTitle("Easter Sunday(Several states)");
        obj26.setDay("Sunday");
        obj26.setDate("12 April 2020");
        arrayList.add(obj26);

        ModelHoliday obj27=new ModelHoliday();
        obj27.setTitle("Bohag Bihu(Assam)");
        obj27.setDay("Monday");
        obj27.setDate("13 April 2020");
        arrayList.add(obj27);


        ModelHoliday obj27A=new ModelHoliday();
        obj27A.setTitle("Vaisakh(Haryana,Jharkhand,Punjab)");
        obj27A.setDay("Monday");
        obj27A.setDate("13 April 2020");
        arrayList.add(obj27A);

        ModelHoliday obj28=new ModelHoliday();
        obj28.setTitle("Vishu(Kerala)");
        obj28.setDay("Tuesday");
        obj28.setDate("14 April 2020");
        arrayList.add(obj28);


        ModelHoliday obj28A=new ModelHoliday();
        obj28A.setTitle("Ashoka's Birthday Anniversary(Bihar)");
        obj28A.setDay("Tuesday");
        obj28A.setDate("14 April 2020");
        arrayList.add(obj28A);


        ModelHoliday obj28B=new ModelHoliday();
        obj28B.setTitle("Bohag Bihu(Arunachal Pradesh)");
        obj28B.setDay("Tuesday");
        obj28B.setDate("14 April 2020");
        arrayList.add(obj28B);


        ModelHoliday obj28C=new ModelHoliday();
        obj28C.setTitle("Bengali New Year(West Bengal,Tripura)");
        obj28C.setDay("Tuesday");
        obj28C.setDate("14 April 2020");
        arrayList.add(obj28C);


        ModelHoliday obj28D=new ModelHoliday();
        obj28D.setTitle("Puthandu(Tamil Nadu)");
        obj28D.setDay("Tuesday");
        obj28D.setDate("14 April 2020");
        arrayList.add(obj28D);


        ModelHoliday obj28E=new ModelHoliday();
        obj28E.setTitle("Mahabishubha Sankranti(Odisha)");
        obj28E.setDay("Tuesday");
        obj28E.setDate("14 April 2020");
        arrayList.add(obj28E);


        ModelHoliday obj28F=new ModelHoliday();
        obj28F.setTitle("Dr.B.R Ambedkar Jayanti(Several states)");
        obj28F.setDay("Tuesday");
        obj28F.setDate("14 April 2020");
        arrayList.add(obj28);

        ModelHoliday obj29=new ModelHoliday();
        obj29.setTitle("Himachal Day(Himachal Pradesh)");
        obj29.setDay("Wednesday");
        obj29.setDate("15 April 2020");
        arrayList.add(obj29);

        ModelHoliday obj30=new ModelHoliday();
        obj30.setTitle("Vaisakhi(Several states)");
        obj30.setDay("Tuesday");
        obj30.setDate("21 April 2020");
        arrayList.add(obj30);

        ModelHoliday obj31=new ModelHoliday();
        obj31.setTitle("Maharshi Parasuram Jayanti(Karnataka)");
        obj31.setDay("Saturday");
        obj31.setDate("25 April 2020");
        arrayList.add(obj31);


        ModelHoliday obj31A=new ModelHoliday();
        obj31A.setTitle("Basava Jayanti(Karnataka)");
        obj31A.setDay("Saturday");
        obj31A.setDate("25 April 2020");
        arrayList.add(obj31A);

        ModelHoliday obj32=new ModelHoliday();
        obj32.setTitle("Labour Day(Several states)");
        obj32.setDay("Friday");
        obj32.setDate("01 May 2020");
        arrayList.add(obj32);

        ModelHoliday obj32A=new ModelHoliday();
        obj32A.setTitle("Maharastra Day(Maharastra)");
        obj32A.setDay("Friday");
        obj32A.setDate("01 May 2020");
        arrayList.add(obj32A);

        ModelHoliday obj33=new ModelHoliday();
        obj33.setTitle("Buddha Purnima(Several states)");
        obj33.setDay("Thursday");
        obj33.setDate("07 May 2020");
        arrayList.add(obj33);

        ModelHoliday obj34=new ModelHoliday();
        obj34.setTitle("Guru Rabindranath Jayanti(West Bengal,Tripura)");
        obj34.setDay("Friday");
        obj34.setDate("08 May 2020");
        arrayList.add(obj34);

        ModelHoliday obj35=new ModelHoliday();
        obj35.setTitle("State Day(Sikkim)");
        obj35.setDay("Saturday");
        obj35.setDate("16 May 2020");
        arrayList.add(obj35);

        ModelHoliday obj36=new ModelHoliday();
        obj36.setTitle("Shab-i-Qadr(Jammu and Kashmir)");
        obj36.setDay("Wednesday");
        obj36.setDate("20 May 2020");
        arrayList.add(obj36);

        ModelHoliday obj37=new ModelHoliday();
        obj37.setTitle("Jamatul Bidah(Jammu and Kasmir)");
        obj37.setDay("Friday");
        obj37.setDate("22 May 2020");
        arrayList.add(obj37);

        ModelHoliday obj38=new ModelHoliday();
        obj38.setTitle("Eid-al-Fitr(Several states)");
        obj38.setDay("Sunday");
        obj38.setDate("24 May 2020");
        arrayList.add(obj38);

        ModelHoliday obj39=new ModelHoliday();
        obj39.setTitle("Maharana Pradap Jayanti(Several states)");
        obj39.setDay("Monday");
        obj39.setDate("25 May 2020");
        arrayList.add(obj39);


        ModelHoliday obj40=new ModelHoliday();
        obj40.setTitle("Sant Guru Kabir Jayanti(Several states)");
        obj40.setDay("Friday");
        obj40.setDate("05 June 2020");
        arrayList.add(obj40);

        ModelHoliday obj41=new ModelHoliday();
        obj41.setTitle("Raja Sankranti(Odisha)");
        obj41.setDay("Monday");
        obj41.setDate("15 June 2020");
        arrayList.add(obj41);

        ModelHoliday obj42=new ModelHoliday();
        obj42.setTitle("Raja Yatra(Odisha)");
        obj42.setDay("Tuesday");
        obj42.setDate("23 June 2020");
        arrayList.add(obj42);

        ModelHoliday obj43=new ModelHoliday();
        obj43.setTitle("Kharchi Puja(Tripura)");
        obj43.setDay("Sunday");
        obj43.setDate("28 June 2020");
        arrayList.add(obj43);

        ModelHoliday obj44=new ModelHoliday();
        obj44.setTitle("Behdienkhlam(Meghalaya)");
        obj44.setDay("Friday");
        obj44.setDate("03 July 2020");
        arrayList.add(obj44);


        ModelHoliday obj45=new ModelHoliday();
        obj45.setTitle("Guru Purnima Festival(Several states)");
        obj45.setDay("Sunday");
        obj45.setDate("05 July 2020");
        arrayList.add(obj45);


        ModelHoliday obj46=new ModelHoliday();
        obj46.setTitle("MHIP Day(Mizoram)");
        obj46.setDay("Monday");
        obj46.setDate("06 July 2020");
        arrayList.add(obj46);


        ModelHoliday obj47=new ModelHoliday();
        obj47.setTitle("Ker Puja(Tripura)");
        obj47.setDay("Tuesday");
        obj47.setDate("14 July 2020");
        arrayList.add(obj47);


        ModelHoliday obj48=new ModelHoliday();
        obj48.setTitle("Bonalu(Telengana)");
        obj48.setDay("Thursday");
        obj48.setDate("16 July 2020");
        arrayList.add(obj48);


        ModelHoliday obj49=new ModelHoliday();
        obj49.setTitle("Karkkadaka Vavu(Kerala)");
        obj49.setDay("Monday");
        obj49.setDate("20 July 2020");
        arrayList.add(obj49);


        ModelHoliday obj50=new ModelHoliday();
        obj50.setTitle("Haryali Teej(Haryana,Punjab,Chandigarh)");
        obj50.setDay("Thursday");
        obj50.setDate("23 July 2020");
        arrayList.add(obj50);


        ModelHoliday obj51=new ModelHoliday();
        obj51.setTitle("Bakrid/Eid-al-Adha(Several states)");
        obj51.setDay("Friday");
        obj51.setDate("31 July 2020");
        arrayList.add(obj51);



        ModelHoliday obj52=new ModelHoliday();
        obj52.setTitle("Bakrid(Several states)");
        obj52.setDay("Saturday");
        obj52.setDate("01 August 2020");
        arrayList.add(obj52);

        ModelHoliday obj53=new ModelHoliday();
        obj53.setTitle("Raksha Bandhan(Several states)");
        obj53.setDay("Monday");
        obj53.setDate("03 August 2020");
        arrayList.add(obj53);

        ModelHoliday obj54=new ModelHoliday();
        obj54.setTitle("Janmashtami(Several states)");
        obj54.setDay("Tuesday");
        obj54.setDate("11 August 2020");
        arrayList.add(obj54);

        ModelHoliday obj55=new ModelHoliday();
        obj55.setTitle("Independence Day(All over India)");
        obj55.setDay("Saturday");
        obj55.setDate("15 August 2020");
        arrayList.add(obj55);

        ModelHoliday obj56=new ModelHoliday();
        obj56.setTitle("Parsi New Year(Maharastra and Gujarat)");
        obj56.setDay("Monday");
        obj56.setDate("17 August 2020");
        arrayList.add(obj56);

        ModelHoliday obj57=new ModelHoliday();
        obj57.setTitle("Ganesh Chaturthi(Several states)");
        obj57.setDay("Saturday");
        obj57.setDate("22 August 2020");
        arrayList.add(obj57);

        ModelHoliday obj58=new ModelHoliday();
        obj58.setTitle("Nuakhai(Odisha)");
        obj58.setDay("Sunday");
        obj58.setDate("23 August 2020");
        arrayList.add(obj58);

        ModelHoliday obj59=new ModelHoliday();
        obj59.setTitle("Ramdev Jyanti(Rajasthan)");
        obj59.setDay("Friday");
        obj59.setDate("28 August 2020");
        arrayList.add(obj59);

        ModelHoliday obj59A=new ModelHoliday();
        obj59A.setTitle("Ayyankali Jayanti(Kerala)");
        obj59A.setDay("Friday");
        obj59A.setDate("28 August 2020");
        arrayList.add(obj59A);

        ModelHoliday obj59B=new ModelHoliday();
        obj59B.setTitle("Teja Dashami(Rajasthan)");
        obj59B.setDay("Friday");
        obj59B.setDate("28 August 2020");
        arrayList.add(obj59B);

        ModelHoliday obj59C=new ModelHoliday();
        obj59C.setTitle("Ashura(Several states)");
        obj59C.setDay("Friday");
        obj59C.setDate("28 August 2020");
        arrayList.add(obj59C);


        ModelHoliday obj60=new ModelHoliday();
        obj60.setTitle("Muharram(Several states)");
        obj60.setDay("Sunday");
        obj60.setDate("30 August 2020");
        arrayList.add(obj60);

        ModelHoliday obj61=new ModelHoliday();
        obj61.setTitle("Thiruvonam(Kerala)");
        obj61.setDay("Monday");
        obj61.setDate("31 August 2020");
        arrayList.add(obj61);

        ModelHoliday obj62=new ModelHoliday();
        obj62.setTitle("Indra Jatra(Sikkim)");
        obj62.setDay("Tuesday");
        obj62.setDate("01 September 2020");
        arrayList.add(obj62);

        ModelHoliday obj63=new ModelHoliday();
        obj63.setTitle("Sree Narayana Guru Jayanti(Kerala)");
        obj63.setDay("Wednesday");
        obj63.setDate("02 September 2020");
        arrayList.add(obj63);

        ModelHoliday obj64=new ModelHoliday();
        obj64.setTitle("Viswakarma Day(Several states)");
        obj64.setDay("Wednesday");
        obj64.setDate("16 September 2020");
        arrayList.add(obj64);

        ModelHoliday obj65=new ModelHoliday();
        obj65.setTitle("Mahalaya Amavasi(Several states)");
        obj65.setDay("Thursday");
        obj65.setDate("17 September 2020");
        arrayList.add(obj65);


        ModelHoliday obj66=new ModelHoliday();
        obj66.setTitle("Gandhi Jayanti(All over India)");
        obj66.setDay("Friday");
        obj66.setDate("02 Ocotober 2020");
        arrayList.add(obj66);

        ModelHoliday obj67=new ModelHoliday();
        obj67.setTitle("Bathukamma(Telangana and Andra Pradesh)");
        obj67.setDay("Saturday");
        obj67.setDate("17 Ocotober 2020");
        arrayList.add(obj67);


        ModelHoliday obj67A=new ModelHoliday();
        obj67A.setTitle("Maharaja Agrasen Jayanti(Punjab,Haryana,Uttar Pradesh,Rajasthan)");
        obj67A.setDay("Saturday");
        obj67A.setDate("17 Ocotober 2020");
        arrayList.add(obj67A);

        ModelHoliday obj68=new ModelHoliday();
        obj68.setTitle("Maha Saptami(Several states)");
        obj68.setDay("Friday");
        obj68.setDate("23 Ocotober 2020");
        arrayList.add(obj68);

        ModelHoliday obj69=new ModelHoliday();
        obj69.setTitle("Maha Ashtami(Several states)");
        obj69.setDay("Saturday");
        obj69.setDate("24 Ocotober 2020");
        arrayList.add(obj69);

        ModelHoliday obj70=new ModelHoliday();
        obj70.setTitle("Maha Navami(Several states)");
        obj70.setDay("Sunday");
        obj70.setDate("25 Ocotober 2020");
        arrayList.add(obj70);

        ModelHoliday obj71=new ModelHoliday();
        obj71.setTitle("Vijaya Dashami(Several states)");
        obj71.setDay("Monday");
        obj71.setDate("26 Ocotober 2020");
        arrayList.add(obj71);

        ModelHoliday obj72=new ModelHoliday();
        obj72.setTitle("Eid-e-Milad(Mawlid)(Several states)");
        obj72.setDay("Thursday");
        obj72.setDate("29 Ocotober 2020");
        arrayList.add(obj71);

        ModelHoliday obj73=new ModelHoliday();
        obj73.setTitle("Lakshmi Puja(West Bengal,Tripura,Odisha)");
        obj73.setDay("Saturday");
        obj73.setDate("31 Ocotober 2020");
        arrayList.add(obj73);


        ModelHoliday obj73A=new ModelHoliday();
        obj73A.setTitle("Maharishi Valmiki Jayanti(Several states)");
        obj73A.setDay("Saturday");
        obj73A.setDate("31 Ocotober 2020");
        arrayList.add(obj73A);

        ModelHoliday obj74=new ModelHoliday();
        obj74.setTitle("Kannada Rajyotsava(Karnataka)");
        obj74.setDay("Sunday");
        obj74.setDate("01 November 2020");
        arrayList.add(obj74);

        ModelHoliday obj74A=new ModelHoliday();
        obj74A.setTitle("Kerala Piravi(Kerala)");
        obj74A.setDay("Sunday");
        obj74A.setDate("01 November 2020");
        arrayList.add(obj74A);

        ModelHoliday obj74B=new ModelHoliday();
        obj74B.setTitle("Puducherry Liberation Day(Puducherry)");
        obj74B.setDay("Sunday");
        obj74B.setDate("01 November 2020");
        arrayList.add(obj74B);

        ModelHoliday obj74C=new ModelHoliday();
        obj74C.setTitle("Haryana Day(Haryana)");
        obj74C.setDay("Sunday");
        obj74C.setDate("01 November 2020");
        arrayList.add(obj74C);



        ModelHoliday obj75=new ModelHoliday();
        obj75.setTitle("Diwali/Deepavali(Several states)");
        obj75.setDay("Saturday");
        obj75.setDate("14 November 2020");
        arrayList.add(obj75);


        ModelHoliday obj75A=new ModelHoliday();
        obj75A.setTitle("Narak Chaturdashi(Several states)");
        obj75A.setDay("Saturday");
        obj75A.setDate("14 November 2020");
        arrayList.add(obj75A);

        ModelHoliday obj76=new ModelHoliday();
        obj76.setTitle("Govardhan Puja(Several states)");
        obj76.setDay("Sunday");
        obj76.setDate("15 November 2020");
        arrayList.add(obj76);

        ModelHoliday obj77=new ModelHoliday();
        obj77.setTitle("Bhai Dooj(Several states)");
        obj77.setDay("Monday");
        obj77.setDate("16 November 2020");
        arrayList.add(obj77);

        ModelHoliday obj77A=new ModelHoliday();
        obj77A.setTitle("Vikram Samvat New Year(Several states)");
        obj77A.setDay("Monday");
        obj77A.setDate("16 November 2020");
        arrayList.add(obj77A);

        ModelHoliday obj78=new ModelHoliday();
        obj78.setTitle("Chhath Puja(Jharkhand,Bihar)");
        obj78.setDay("Friday");
        obj78.setDate("20 November 2020");
        arrayList.add(obj78);

        ModelHoliday obj79=new ModelHoliday();
        obj79.setTitle("Martyrdom Day of Sri Guru Teg Bahaur(Several states)");
        obj79.setDay("Tuesday");
        obj79.setDate("24 November 2020");
        arrayList.add(obj79);

        ModelHoliday obj80=new ModelHoliday();
        obj80.setTitle("Guru Nanak Jayanti(Several states)");
        obj80.setDay("Monday");
        obj80.setDate("30 November 2020");
        arrayList.add(obj80);


        ModelHoliday obj81=new ModelHoliday();
        obj81.setTitle("Feast of St.Francis Xavier(Several states)");
        obj81.setDay("Thursday");
        obj81.setDate("03 December 2020");
        arrayList.add(obj81);


        ModelHoliday obj82=new ModelHoliday();
        obj82.setTitle("Hanukkah(Several states)");
        obj82.setDay("Monday");
        obj82.setDate("10 December 2020");
        arrayList.add(obj82);


        ModelHoliday obj83=new ModelHoliday();
        obj83.setTitle("Guru Ghasidas Jayanti(Goa)");
        obj83.setDay("Friday");
        obj83.setDate("18 December 2020");
        arrayList.add(obj83);


        ModelHoliday obj84=new ModelHoliday();
        obj84.setTitle("Liberation Day(Goa)");
        obj84.setDay("Saturday");
        obj84.setDate("19 December 2020");
        arrayList.add(obj84);


        ModelHoliday obj85=new ModelHoliday();
        obj85.setTitle("Christmas(All over India)");
        obj85.setDay("Saturday");
        obj85.setDate("25 December 2020");
        arrayList.add(obj85);


        ModelHoliday obj86=new ModelHoliday();
        obj86.setTitle("U Kiang Nangbah(Meghalaya)");
        obj86.setDay("Wednesday");
        obj86.setDate("30 December 2020");
        arrayList.add(obj86);

        return arrayList;
    }
}