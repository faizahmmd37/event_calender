package com.nightowlstudios.mynotepad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CalenderMainActivity extends AppCompatActivity {
    mysqLiteOpenHelper mysqliteopenhelper;
    customCalenderView customcalenderview;
    BottomNavigationView bottomnavcalender;
    TextView currentdatex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_main);
        bottomnavcalender=findViewById(R.id.bottomnavcalender);
        currentdatex=findViewById(R.id.currentdate);
        customcalenderview=(customCalenderView)findViewById(R.id.custom_calender);
        mysqliteopenhelper=new mysqLiteOpenHelper(this);

        SimpleDateFormat simple=new SimpleDateFormat();
        String todaydate=simple.format(new Date());
        currentdatex.setText(todaydate);

        LayoutInflater layoutInflater=getLayoutInflater();
        View v=layoutInflater.inflate(R.layout.custom_toast,(ViewGroup)findViewById(R.id.custom_toast_layout));

        Toast toast=new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.NO_GRAVITY,0,0);
        toast.setView(v);
        toast.show();

        bottomnavcalender.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.back:

                    case R.id.home1:

                        Intent intent1=new Intent(CalenderMainActivity.this,NavigationActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.exitapp:

                        callAlertbar();

                        break;

                }

                return true;
            }
        });

    }
    public void callAlertbar(){
        AlertDialog.Builder alertbar=new AlertDialog.Builder(this);
        alertbar.setTitle("EXIT APP");
        alertbar.setMessage("Do you really want to exit?");
        alertbar.setCancelable(true);
        alertbar.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        alertbar.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertbar.create();
        alertbar.show();
    }
}