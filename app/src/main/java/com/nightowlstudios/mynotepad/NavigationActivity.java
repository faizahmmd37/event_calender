package com.nightowlstudios.mynotepad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class NavigationActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    FrameLayout frameLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
       //getSupportActionBar().hide();
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        navigationView = findViewById(R.id.navigationview);
        drawerLayout = findViewById(R.id.drawerlayout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(NavigationActivity.this, drawerLayout,toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        getSupportFragmentManager().beginTransaction().replace(R.id.container,new BlankFragment()).commit();
        navigationView.setCheckedItem(R.id.shareitem);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            Fragment object;
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId())
                {
                    case R.id.shareitem:
                       // object=new BlankFragment();
                        Intent sendintent=new Intent();
                        sendintent.setAction(Intent.ACTION_SEND);
                        final String apppackagename="com.nightowlstudios.mynotepad";
                        sendintent.putExtra(Intent.EXTRA_TEXT,"https://play.google.com/store/apps/details?id="+apppackagename);
                        sendintent.setType("text/plain");
                        Intent.createChooser(sendintent,"Share via");
                        startActivity(sendintent);
                        break;
                    case R.id.aboutitem:
                        AlertDialog.Builder aboutdialogue=new AlertDialog.Builder(NavigationActivity.this);
                        aboutdialogue.setCancelable(true);
                        aboutdialogue.setTitle("About us");
                        aboutdialogue.setMessage(R.string.aboutus);
                        aboutdialogue.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                              dialogInterface.dismiss();
                            }
                        });
                        aboutdialogue.create();
                        aboutdialogue.show();
break;

                    case R.id.termsofserviceitem:
                        AlertDialog.Builder termsdialogue=new AlertDialog.Builder(NavigationActivity.this);
                        termsdialogue.setCancelable(true);
                        termsdialogue.setTitle("Terms of services");
                        termsdialogue.setMessage(R.string.termsofservice);
                        termsdialogue.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        termsdialogue.create();
                        termsdialogue.show();
                        break;
                    case
     R.id.contactitem:
                        Toast.makeText(NavigationActivity.this,"opening gmail ",Toast.LENGTH_LONG).show();
                        Intent contactintent=new Intent(Intent.ACTION_SEND);
                    String[] recipients={"faizahmmd37@gmail.com"};
                    String[] cc_recipient={"nightowlstudioz@gmail.com"};
                    contactintent.putExtra(Intent.EXTRA_CC,cc_recipient);
                    contactintent.putExtra(Intent.EXTRA_EMAIL,recipients);
                    contactintent.putExtra(Intent.EXTRA_SUBJECT,"Contact us form");
                    contactintent.putExtra(Intent.EXTRA_TEXT,"Write your message here...");

                    contactintent.setType("text/html");
                    contactintent.setPackage("com.google.android.gm");
                    startActivity(Intent.createChooser(contactintent,"send email"));
                    break;
                    case R.id.faqitem:
                        object=new FAQfragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.container,object).commit();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                }

                //getSupportFragmentManager().beginTransaction().replace(R.id.container,object).commit();
               drawerLayout.closeDrawer(GravityCompat.START);
                return true;

            }
        });
    }

    @Override
    public void onBackPressed() {

        //super.onBackPressed();
        CallAlertBAR();

    }
    public void CallAlertBAR(){
        AlertDialog.Builder alertBar=new AlertDialog.Builder(this);
        alertBar.setTitle("EXIT");
        alertBar.setMessage("Do you really want to exit?");
        alertBar.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertBar.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
               // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
               // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        alertBar.create();
        alertBar.show();
    }
}