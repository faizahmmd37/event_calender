package com.nightowlstudios.mynotepad;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class customCalenderView extends LinearLayout {
    ImageButton imgbtn1,imgbtn2;
    TextView txtmonth;
    GridView gridView;
    public static final int MAX_CALENDER_DAYS=42;
    Calendar calendar=Calendar.getInstance(Locale.ENGLISH);
    Context context;
    mysqLiteOpenHelper mysqliteopenhelper;

    List<Date> dates=new ArrayList<>();
    List<Events> eventsList=new ArrayList<>();
    int alarmYear,alarmMonth,alarmDay,alarmHour,alarmMinute;

    SimpleDateFormat dateFormat=new SimpleDateFormat("MMMMyyyy",Locale.ENGLISH);
    SimpleDateFormat monthFormat=new SimpleDateFormat("MMMM",Locale.ENGLISH);
    SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy",Locale.ENGLISH);
    SimpleDateFormat eventDateFormat=new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);


    MyGridAdapter myGridAdapter;
    AlertDialog alertDialog;

    public customCalenderView(Context context) {
        super(context);
    }

    public customCalenderView(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context=context;
        initializeLayout();
        setUpCalender();
        imgbtn1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                calendar.add(Calendar.MONTH,-1);
                setUpCalender();
            }
        });
        imgbtn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                calendar.add(Calendar.MONTH,1);
                setUpCalender();
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setCancelable(true);
                final View addView=LayoutInflater.from(parent.getContext()).inflate(R.layout.add_newevent_layout,null);
                final EditText EventName=addView.findViewById(R.id.eventname);
                final TextView EventTime=addView.findViewById(R.id.eventtime);
                ImageButton SetTime=addView.findViewById(R.id.seteventtime);
                final CheckBox alarmMe=addView.findViewById(R.id.alarmme);
                Calendar dateCalendar=Calendar.getInstance();
                dateCalendar.setTime(dates.get(position));
                alarmYear=dateCalendar.get(Calendar.YEAR);
                alarmMonth=dateCalendar.get(Calendar.MONTH);
                alarmDay=dateCalendar.get(Calendar.DAY_OF_MONTH);



                Button AddEvent=addView.findViewById(R.id.addevent);
                SetTime.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar calendar=Calendar.getInstance();
                        int hours=calendar.get(Calendar.HOUR_OF_DAY);
                        int minutes=calendar.get(Calendar.MINUTE);
                        TimePickerDialog timePickerDialog=new TimePickerDialog(addView.getContext(), R.style.Theme_AppCompat_Dialog, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                Calendar c=Calendar.getInstance();
                                c.set(Calendar.HOUR_OF_DAY,hourOfDay);
                                c.set(Calendar.MINUTE,minute);
                                c.setTimeZone(TimeZone.getDefault());
                                SimpleDateFormat hformate=new SimpleDateFormat("K:mm a",Locale.ENGLISH);
                                String event_Time=hformate.format(c.getTime());
                                EventTime.setText(event_Time);
                                alarmHour=c.get(Calendar.HOUR_OF_DAY);
                                alarmMinute=c.get(Calendar.MINUTE);

                            }
                        },hours,minutes,false);
                        timePickerDialog.show();
                    }
                });
                final String date=eventDateFormat.format(dates.get(position));
                final String month=monthFormat.format(dates.get(position));
                final String year=yearFormat.format(dates.get(position));
                AddEvent.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (alarmMe.isChecked()){
                            SaveEvent(EventName.getText().toString(),EventTime.getText().toString(),date,month,year,"on");
                            setUpCalender();
                            Calendar calendar=Calendar.getInstance();
                            calendar.set(alarmYear,alarmMonth,alarmDay,alarmHour,alarmMinute);
                            setAlarm(calendar,EventName.getText().toString(),EventTime.getText().toString(),getRequestCode(date,EventName.getText().toString(),EventTime.getText().toString()));
                            alertDialog.dismiss();
                        }else {
                            SaveEvent(EventName.getText().toString(),EventTime.getText().toString(),date,month,year,"off");
                            setUpCalender();
                            alertDialog.dismiss();
                        }

                    }
                });
                builder.setView(addView);
                alertDialog=builder.create();
                alertDialog.show();
            }
        });
     gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
         @Override
         public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
             String date=eventDateFormat.format(dates.get(position));

             AlertDialog.Builder builder=new AlertDialog.Builder(context);
             builder.setCancelable(true);
             View showView=LayoutInflater.from(parent.getContext()).inflate(R.layout.show_events_layout,null);
             RecyclerView recyclerView=showView.findViewById(R.id.EventsRV);
             RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(showView.getContext());
             recyclerView.setLayoutManager(layoutManager);
             recyclerView.setHasFixedSize(true);
             EventRecyclerAdapter eventRecyclerAdapter=new EventRecyclerAdapter(showView.getContext(),CollectEventsByDate(date));
             recyclerView.setAdapter(eventRecyclerAdapter);
             eventRecyclerAdapter.notifyDataSetChanged();

             builder.setView(showView);
             alertDialog=builder.create();
             alertDialog.show();
             alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                 @Override
                 public void onCancel(DialogInterface dialogInterface) {
                     setUpCalender();
                 }
             });
             return true;
         }
     });
    }
    private int getRequestCode(String date,String event,String time){
        int code=0;
        mysqliteopenhelper=new mysqLiteOpenHelper(context);
        SQLiteDatabase database=mysqliteopenhelper.getReadableDatabase();
        Cursor cursor=mysqliteopenhelper.readIDEvents(date,event,time,database);
        while (cursor.moveToNext())
        {
             code=cursor.getInt(cursor.getColumnIndex(DBStructure.ID));


        }
        cursor.close();
        mysqliteopenhelper.close();
        return code;
    }
    private void setAlarm(Calendar calendar,String event,String time,int RequestCode){
        Intent intent=new Intent(context.getApplicationContext(),AlarmReceiver.class);
        intent.putExtra("event",event);
        intent.putExtra("time",time);
        intent.putExtra("id",RequestCode);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(context,RequestCode,intent,PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager=(AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);

    }
    private ArrayList<Events> CollectEventsByDate(String date){
        ArrayList<Events> arrayList=new ArrayList<>();
        mysqliteopenhelper=new mysqLiteOpenHelper(context);
        SQLiteDatabase database=mysqliteopenhelper.getReadableDatabase();
        Cursor cursor=mysqliteopenhelper.readEvents(date,database);
        while (cursor.moveToNext())
        {
            String event=cursor.getString(cursor.getColumnIndex(DBStructure.EVENT));
            String time=cursor.getString(cursor.getColumnIndex(DBStructure.TIME));
            String Date=cursor.getString(cursor.getColumnIndex(DBStructure.DATE));
            String month=cursor.getString(cursor.getColumnIndex(DBStructure.MONTH));
            String Year=cursor.getString(cursor.getColumnIndex(DBStructure.YEAR));
            Events events=new Events(event,time,Date,month,Year);
            arrayList.add(events);
        }
        cursor.close();
        mysqliteopenhelper.close();
        return arrayList;
    }
    public customCalenderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    private void SaveEvent(String event, String time, String date, String month, String year,String notify){
        mysqliteopenhelper=new mysqLiteOpenHelper(context);
        SQLiteDatabase database=mysqliteopenhelper.getWritableDatabase();
        mysqliteopenhelper.saveEvent(event,time,date,month,year,notify,database);
        mysqliteopenhelper.close();
        Toast.makeText(context,"Event Saved",Toast.LENGTH_LONG).show();
    }
    public void initializeLayout(){
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=layoutInflater.inflate(R.layout.calender_layout,this);
        imgbtn1=v.findViewById(R.id.imagebtn1);
        imgbtn2=v.findViewById(R.id.imagebtn2);
        txtmonth=v.findViewById(R.id.textmonth);
        gridView=v.findViewById(R.id.gridview);
    }
    public void setUpCalender(){
        String currentdate=dateFormat.format(calendar.getTime());
        txtmonth.setText(currentdate);
        dates.clear();
        Calendar monthCalender=(Calendar)calendar.clone();
        monthCalender.set(Calendar.DAY_OF_MONTH,1);
        int FirstDayofMonth=monthCalender.get(Calendar.DAY_OF_WEEK)-1;
        monthCalender.add(Calendar.DAY_OF_MONTH,-FirstDayofMonth);
        CollectEventsPerMonth(monthFormat.format(calendar.getTime()),yearFormat.format(calendar.getTime()));
        while (dates.size()<MAX_CALENDER_DAYS){
            dates.add(monthCalender.getTime());
            monthCalender.add(Calendar.DAY_OF_MONTH,1);
        }
        myGridAdapter=new MyGridAdapter(context,dates,calendar,eventsList);
        gridView.setAdapter(myGridAdapter);
    }
    private void CollectEventsPerMonth(String Month,String year){
        eventsList.clear();
        mysqliteopenhelper=new mysqLiteOpenHelper(context);
        SQLiteDatabase database=mysqliteopenhelper.getReadableDatabase();
        Cursor cursor=mysqliteopenhelper.readEventPerMonth(Month,year,database);
        while (cursor.moveToNext()){
            String event=cursor.getString(cursor.getColumnIndex(DBStructure.EVENT));
            String time=cursor.getString(cursor.getColumnIndex(DBStructure.TIME));
            String date=cursor.getString(cursor.getColumnIndex(DBStructure.DATE));
            String month=cursor.getString(cursor.getColumnIndex(DBStructure.MONTH));
            String Year=cursor.getString(cursor.getColumnIndex(DBStructure.YEAR));
            Events events=new Events(event,time,date,month,Year);
            eventsList.add(events);

        }
        cursor.close();
        mysqliteopenhelper.close();

    }
}
