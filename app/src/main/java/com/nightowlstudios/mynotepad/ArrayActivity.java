package com.nightowlstudios.mynotepad;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashSet;


public class ArrayActivity extends AppCompatActivity {
    SwipeMenuListView listView;
    static ArrayList<String> arrayList;
    static ArrayAdapter<String> arrayAdapter;
BottomNavigationView bottomNavigationView;
HashSet<String> set;
static myAdapter myadapter;
Toolbar toolbar2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_array);
        listView = (SwipeMenuListView) findViewById(R.id.listView);
        listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        toolbar2=findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar2);
        swipe();

        bottomNavigationView=findViewById(R.id.bottomnavigationview);
        FloatingActionButton floatingActionButton=findViewById(R.id.floatingbtn);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ArrayActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


       // HashSet<String>
                set = (HashSet<String>) getSharedPreferences("sharedpreferencekey", MODE_PRIVATE).getStringSet("key",null);
        if (set == null) {
            arrayList = new ArrayList<>();
            arrayList.add("Write your first note here");

        } else {
            arrayList = new ArrayList(set);
        }


      //arrayAdapter = new ArrayAdapter<String>(this, R.layout.singlerow, arrayList);
        myadapter=new myAdapter(arrayList,ArrayActivity.this);
listView.setAdapter(myadapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("intentkey", i);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int itemtodeleteid = i;
                AlertDialog.Builder builder = new AlertDialog.Builder(ArrayActivity.this);
                builder.setMessage("Do you want to delete");
                builder.setTitle("delete");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        arrayList.remove(itemtodeleteid);
                        ArrayActivity.myadapter.notifyDataSetChanged();
                        Toast.makeText(ArrayActivity.this,"Note deleted",Toast.LENGTH_LONG).show();

                        SharedPreferences sharedPreferences = getSharedPreferences("sharedpreferencekey", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        HashSet<String> set = new HashSet<>(ArrayActivity.arrayList);
                        editor.putStringSet("key", set);
                        editor.apply();

                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create();
                builder.show();
                return true;


            }
        });


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.back:

                    case R.id.home1:

                        Intent intent1=new Intent(ArrayActivity.this,NavigationActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.exitapp:

                     callAlertbar();

                        break;

                }

                return true;
            }
        });

    }
    public void callAlertbar(){
        AlertDialog.Builder alertbar=new AlertDialog.Builder(this);
        alertbar.setTitle("EXIT APP");
        alertbar.setMessage("Do you really want to exit?");
        alertbar.setCancelable(true);
        alertbar.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        alertbar.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertbar.create();
        alertbar.show();
    }
    public void swipe(){
        SwipeMenuCreator swipeMenuCreator=new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem swipeMenuItem=new SwipeMenuItem(getApplicationContext());
                swipeMenuItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                swipeMenuItem.setWidth(180);
                swipeMenuItem.setIcon(R.drawable.ic_baseline_delete_24);
                menu.addMenuItem(swipeMenuItem);
            }
        };
        listView.setMenuCreator(swipeMenuCreator);
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ArrayActivity.this);
                builder.setMessage("Do you want to delete");
                builder.setTitle("delete");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        arrayList.remove(position);
                        ArrayActivity.myadapter.notifyDataSetChanged();

                        SharedPreferences sharedPreferences = getSharedPreferences("sharedpreferencekey", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        HashSet<String> set = new HashSet<>(ArrayActivity.arrayList);
                        editor.putStringSet("key", set);
                        editor.apply();
                        Toast.makeText(ArrayActivity.this,"Note deleted",Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create();
                builder.show();

                return true;
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu){
getMenuInflater().inflate(R.menu.searchmenu,menu);
MenuItem menuItem=menu.findItem(R.id.search);
        SearchView searchView=(SearchView)menuItem.getActionView();
        searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.queryhintnote) + "</font>"));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
myadapter.getFilter().filter(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


}